import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import axios from 'axios';
import './EditTask.css';

const EditTask = () => {
  const { taskId } = useParams();
  const navigate = useNavigate();
  const [taskName, setTaskName] = useState('');
  const [oldTaskName, setOldTaskName] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  
  useEffect(() => {
    // Fetch the task details from the API or state
    const fetchTask = async () => {
      try {
        const response = await axios.get(`/api/task/${taskId}`);
        setTaskName(response.data.name);
        setOldTaskName(response.data.name);
      } catch (error) {
        console.error('Error fetching task:', error);
      }
    };
    
    fetchTask();
  }, [taskId]);

  const handleUpdate = async (event) => {
    event.preventDefault();
    if (!taskName.trim()) {
      setErrorMessage('Task name cannot be empty.');
      return;
    }
    try {
      await axios.put(`/api/task/${taskId}`, { name: taskName });
      navigate('/');
    } catch (error) {
      console.error('Error updating task:', error);
    }
  };

  const handleDelete = async () => {
    if (window.confirm('Are you sure you want to delete this task?')) {
      try {
        await axios.delete(`/api/task/${taskId}`);
        navigate('/');
      } catch (error) {
        console.error('Error deleting task:', error);
        setErrorMessage('Failed to delete task');
      }
    }
  };

  return (
    <div className="edit-task-container">
      <h1>Edit Task</h1>
      <div className="current-task-name">
        <strong>Current Task Name:</strong> {oldTaskName}
      </div>
      <form onSubmit={handleUpdate} className="edit-task-form">
        <input 
          type="text" 
          value={taskName} 
          onChange={(e) => setTaskName(e.target.value)} 
          className="task-input"
        />
        {errorMessage && <p className="error-message">{errorMessage}</p>}
        <button type="submit" className="update-button">Update Task</button>
        <button type="button" onClick={handleDelete} className="delete-button">Delete Task</button>
      </form>
    </div>
  );
};

export default EditTask;