import React, { useState, useEffect } from 'react';
import '../App.css';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';

const ToDoList = () => {
  const [todos, setTodos] = useState([]);
  const navigate = useNavigate();
  const tableStyle = {
    marginLeft: 'auto',
    marginRight: 'auto',
  };

  const handleTaskClick = (taskId) => {
    navigate(`/edit-task/${taskId}`);
  };

  useEffect(() => {
    const fetchTodos = async () => {
      try {
        const response = await fetch('./api/tasks');
        if (!response.ok) {
          throw new Error(`HTTP error! status: ${response.status}`);
        }
        const data = await response.json();
        setTodos(data);
      } catch (error) {
        console.error(`Could not fetch todos: ${error}`);
      }
    };

    fetchTodos();
  }, []);

  return (
    <div>
      <div className="header-container">
        <div className="header-spacer"></div>
        <h1>To-Do List</h1>
        <button className="add-button">
          <Link to="/add-task" className="icon-link">
            <i className="fas fa-plus"></i>
          </Link>
        </button>
      </div>
      <table className="centered-table">
        <thead>
          <tr>
            <th>Row</th>
            <th>Task</th>
          </tr>
        </thead>
        <tbody>
          {todos.map((todo, index) => (
            <tr key={todo.id} onClick={() => handleTaskClick(todo.id)}>
              <td>{index + 1}</td>
              <td>{todo.name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ToDoList;