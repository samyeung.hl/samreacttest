const { Pool } = require('pg');
const fs = require('fs');

// Create a new instance of Pool
/*
const pool = new Pool({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT,
  ssl: {
    rejectUnauthorized: true,
    ca: fs.readFileSync('./DigiCertGlobalRootCA.crt.pem').toString()
  }
});
*/
const pool = new Pool({
  user: 'samSqlServerAdmin',
  host: 'samtodolist.postgres.database.azure.com',
  database: 'postgres',
  password: 'PostgreSQLPW#',
  port: '5432',
  ssl: {
      rejectUnauthorized: true,
      ca: fs.readFileSync('./DigiCertGlobalRootCA.crt.pem').toString()
    }
});

module.exports = pool;