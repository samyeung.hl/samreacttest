// AddTask.js

import React, { useState } from 'react';
import axios from 'axios';
import './AddTask.css';
import { useNavigate } from 'react-router-dom';

const AddTask = () => {
  const navigate = useNavigate();
  const [taskName, setTaskName] = useState('');
  const [touched, setTouched] = useState(false);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const trimmedTaskName = taskName.trim();
    if (!trimmedTaskName) {
      console.log('Task name is required');
      return;
    }
    console.log('Task added:', taskName);

    try {
      const response = await axios.post('./api/add-task', { taskName: trimmedTaskName });
      console.log('Task added:', response.data);
      navigate('/');
    } catch (error) {
      console.error('Failed to add task:', error);
    }
  };

  const handleBlur = () => {
    setTouched(true);
  };

  return (
    <div className="add-task-container">
      <h1>Add Task</h1>
      <form onSubmit={handleSubmit} className="task-form">
        <div className="form-group">
          <input
            type="text"
            id="taskName"
            className="task-input"
            placeholder="Enter task name"
            value={taskName}
            onChange={(e) => setTaskName(e.target.value)}
            onBlur={handleBlur}
            required
          />
          {touched && !taskName && (
            <div className="error-message">Task name is required.</div>
          )}
        </div>
        <button type="submit" className="submit-button">Add Task</button>
      </form>
    </div>
  );
};

export default AddTask;