## production url
https://samreacttest.azurewebsites.net/

## public git
https://gitlab.com/samyeung.hl/samreacttest

## node version
v18.19.0

## git clone
https://gitlab.com/samyeung.hl/samreacttest.git

## install libraries after git clone
npm install

## build and run locally
npm run build
npm start