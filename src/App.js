import logo from './logo.svg';
import './App.css';
import ToDoList from './components/toDoList.js';
import AddTask from './components/AddTask.js';
import EditTask from './components/EditTask.js';
import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';


const App = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<ToDoList />} />
        <Route path="/add-task" element={<AddTask />} />
        <Route path="/edit-task/:taskId" element={<EditTask />} />
      </Routes>
    </Router>
  );
};

export default App;
